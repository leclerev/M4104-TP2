package valentin.leclere.channelmessaging.Message;

import java.util.Objects;

public class MessageChannelToken {

    private int userID;
    private String username;
    private String date;
    private String imageUrl;
    private String message;
    private String soundUrl;
    private String latitude;
    private String longitude;

    public int getUserID() {
        return userID;
    }

    public String getUsername() {
        return username;
    }

    public String getMessage() {
        return message;
    }

    public String getDate() {
        return date;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public String toString() {
        return "User ID : " + getUserID() + "\nMessage : " + getMessage();
    }

    @Override
    public boolean equals(Object o) {
        MessageChannelToken mct = new MessageChannelToken();
        if(o instanceof MessageChannelToken)
            mct = (MessageChannelToken) o;
        return (this.getUserID() == mct.getUserID()) && (this.getDate().equals(mct.getDate())) && (this.getMessage().equals(mct.getMessage()));
    }
}
