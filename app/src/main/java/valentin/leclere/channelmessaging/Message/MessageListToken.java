package valentin.leclere.channelmessaging.Message;

import java.util.List;

/**
 * Created by leclerev on 05/02/2018.
 */
public class MessageListToken {

    private List<MessageChannelToken> messages;

    public MessageListToken(){}

    public List<MessageChannelToken> getMessages() {
        return messages;
    }

    @Override
    public String toString() {
        return "Messages : " + getMessages() + "\n";
    }
}
