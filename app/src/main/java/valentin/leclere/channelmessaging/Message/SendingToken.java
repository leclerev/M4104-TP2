package valentin.leclere.channelmessaging.Message;

/**
 * Created by leclerev on 05/02/2018.
 */
public class SendingToken {

    private String response;
    private String code;

    public SendingToken() {}

    public String getResponse() {
        return response;
    }

    public String getCode() {
        return code;
    }
}
