package valentin.leclere.channelmessaging.Message;

public interface OnSendingListener {
    void onSendComplete(String downloadedContent);
    void onSendError(String error);
}
