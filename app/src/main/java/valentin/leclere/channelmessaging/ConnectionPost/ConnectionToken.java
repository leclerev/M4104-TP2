package valentin.leclere.channelmessaging.ConnectionPost;

/**
 * Created by leclerev on 22/01/2018.
 */
public class ConnectionToken {

    private String response;
    private int code;
    private String accesstoken;

    public ConnectionToken() {

    }

    public String getResponse() {
        return response;
    }

    public int getCode() {
        return code;
    }

    public String getAccesstoken() {
        return accesstoken;
    }
}
