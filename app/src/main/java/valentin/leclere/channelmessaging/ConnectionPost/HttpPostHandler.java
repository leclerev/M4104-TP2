package valentin.leclere.channelmessaging.ConnectionPost;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HttpPostHandler extends AsyncTask<PostRequest, Void, String> {
    private ArrayList<OnDownloadListener> listeners = new ArrayList<>();

    public void addOnDownloadListener(OnDownloadListener listener) {
        this.listeners.add(listener);
    }

    @Override
    protected String doInBackground(PostRequest... params) {
        return performPostCall(params[0].getUrl(), params[0].getInfos());
    }

    @Override
    protected void onPostExecute(String param) {
        super.onPostExecute(param);
        for(OnDownloadListener odl : listeners) {
            odl.onDownloadComplete(param);
        }
    }

    public String performPostCall(String requestURL, HashMap<String, String> postDataParams) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);
            HttpURLConnection connec = (HttpURLConnection) url.openConnection();
            connec.setReadTimeout(15000);
            connec.setConnectTimeout(15000);
            connec.setRequestMethod("POST");
            connec.setDoInput(true);
            connec.setDoOutput(true);
            OutputStream os = connec.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));
            writer.flush();
            writer.close();
            os.close();
            int responseCode = connec.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK){
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(connec.getInputStream()));
                while((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private String getPostDataString(HashMap<String, String> params) throws
            UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first) first = false;
            else result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }
}
