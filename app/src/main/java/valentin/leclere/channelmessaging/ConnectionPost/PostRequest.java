package valentin.leclere.channelmessaging.ConnectionPost;

import java.util.HashMap;

public class PostRequest {

    private String url;
    private HashMap<String, String> infos;

    public PostRequest(String url, HashMap<String, String> infos) {
        this.url = url;
        this.infos = infos;
    }

    public String getUrl() {
        return url;
    }

    public HashMap<String, String> getInfos() {
        return infos;
    }

}
