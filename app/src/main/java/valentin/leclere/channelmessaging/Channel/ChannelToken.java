package valentin.leclere.channelmessaging.Channel;

public class ChannelToken {

    private int channelID;
    private String name;
    private int connectedusers;

    public ChannelToken() {}


    public int getChannelID() {
        return channelID;
    }

    public String getName() {
        return name;
    }

    public int getConnectedusers() {
        return connectedusers;
    }

    @Override
    public String toString() {
        return "ID " + channelID + "\nNom : " + name;
    }
}
