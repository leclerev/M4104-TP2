package valentin.leclere.channelmessaging.Channel;

import java.util.List;

/**
 * Created by leclerev on 22/01/2018.
 */
public class ChannelListToken {

    private List<ChannelToken> channels;

    public ChannelListToken(){}

    public List<ChannelToken> getChannels() {
        return channels;
    }

    @Override
    public String toString() {
        String message = "";
        for (ChannelToken ct: channels) {
            message += ct + "\n";
        }
        return message;
    }
}
