package valentin.leclere.channelmessaging;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.HashMap;

import valentin.leclere.channelmessaging.ConnectionPost.HttpPostHandler;
import valentin.leclere.channelmessaging.Fragments.MessageFragment;
import valentin.leclere.channelmessaging.ImagePost.ImagePostHandler;
import valentin.leclere.channelmessaging.ConnectionPost.OnDownloadListener;
import valentin.leclere.channelmessaging.ConnectionPost.PostRequest;
import valentin.leclere.channelmessaging.ImagePost.OnImageDownloadListener;
import valentin.leclere.channelmessaging.Message.MessageChannelToken;
import valentin.leclere.channelmessaging.Message.MessageListToken;
import valentin.leclere.channelmessaging.Message.OnSendingListener;
import valentin.leclere.channelmessaging.Message.SendingHandler;
import valentin.leclere.channelmessaging.Message.SendingToken;

public class MessageActivity extends AppCompatActivity {

    @Override
    public void onCreate(final Bundle savedInstanceSaved) {
        super.onCreate(savedInstanceSaved);
        setContentView(R.layout.activity_message);
    }
}
