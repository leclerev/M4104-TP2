package valentin.leclere.channelmessaging.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.HashMap;

import valentin.leclere.channelmessaging.Channel.ChannelListToken;
import valentin.leclere.channelmessaging.Channel.ChannelToken;
import valentin.leclere.channelmessaging.ChannelListActivity;
import valentin.leclere.channelmessaging.ConnectionPost.HttpPostHandler;
import valentin.leclere.channelmessaging.ConnectionPost.OnDownloadListener;
import valentin.leclere.channelmessaging.ConnectionPost.PostRequest;
import valentin.leclere.channelmessaging.LoginActivity;
import valentin.leclere.channelmessaging.R;

public class ChannelListFragment extends Fragment implements OnDownloadListener {

    private ListView listChannels;
    private ChannelListToken channelListToken;

    public ChannelListToken getChannelListToken() {
        return channelListToken;
    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle savedInstanceState){
        View v = li.inflate(valentin.leclere.channelmessaging.R.layout.channel_list_fragment, vg);
        listChannels = (ListView) v.findViewById(valentin.leclere.channelmessaging.R.id.listView2);
        HttpPostHandler hph = new HttpPostHandler();
        hph.addOnDownloadListener(this);
        SharedPreferences settings = getActivity().getSharedPreferences(LoginActivity.LOGIN_FILE, 0);
        String accessToken = settings.getString("accessToken", "None");
        HashMap<String, String> rqst = new HashMap<>();
        rqst.put("accesstoken", accessToken);
        hph.execute(new PostRequest("http://www.raphaelbischof.fr/messaging/?function=getchannels", rqst));
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        listChannels.setOnItemClickListener((ChannelListActivity) getActivity());
    }

    @Override
    public void onDownloadComplete(String downloadedContent) {
        Gson gson = new Gson();
        channelListToken = gson.fromJson(downloadedContent, ChannelListToken.class);
        listChannels.setAdapter(new ArrayAdapter<ChannelToken>(getActivity(), R.layout.adapter_list_layout, channelListToken.getChannels()){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.adapter_list_layout, parent, false);
                TextView tv1 = (TextView) view.findViewById(R.id.text1);
                tv1.setText(getItem(position).getName());
                tv1.setTextColor(Color.BLACK);
                TextView tv2 = (TextView) view.findViewById(R.id.text2);
                tv2.setText(getString(R.string.nUsersConnectd) + " : " + getItem(position).getConnectedusers());
                tv2.setTextColor(Color.GRAY);
                return view;
            }
        });
    }

    @Override
    public void onDownloadError(String error) {

    }
}
