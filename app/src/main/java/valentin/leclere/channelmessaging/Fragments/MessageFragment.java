package valentin.leclere.channelmessaging.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import valentin.leclere.channelmessaging.BuildConfig;
import valentin.leclere.channelmessaging.Channel.ChannelListToken;
import valentin.leclere.channelmessaging.ChannelListActivity;
import valentin.leclere.channelmessaging.ConnectionPost.HttpPostHandler;
import valentin.leclere.channelmessaging.ConnectionPost.OnDownloadListener;
import valentin.leclere.channelmessaging.ConnectionPost.PostRequest;
import valentin.leclere.channelmessaging.ImagePost.ImagePostHandler;
import valentin.leclere.channelmessaging.ImagePost.OnImageDownloadListener;
import valentin.leclere.channelmessaging.LoginActivity;
import valentin.leclere.channelmessaging.Message.MessageChannelToken;
import valentin.leclere.channelmessaging.Message.MessageListToken;
import valentin.leclere.channelmessaging.Message.OnSendingListener;
import valentin.leclere.channelmessaging.Message.SendingHandler;
import valentin.leclere.channelmessaging.Message.SendingToken;
import valentin.leclere.channelmessaging.MessageActivity;
import valentin.leclere.channelmessaging.R;

public class MessageFragment extends Fragment implements View.OnClickListener, OnSendingListener, OnDownloadListener, OnImageDownloadListener {

    private int channelId;
    private Handler h;
    private Runnable r;
    private boolean mustLoop = true;
    private HttpPostHandler hph;
    private ImagePostHandler iph;
    private String accessToken;
    private ListView messageList;
    private Button envoyerMessage;
    private EditText editText;
    private Bitmap bitmapImage;
    private List<MessageChannelToken> lesMessages = new ArrayList<>();

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    @Override
    public View onCreateView(LayoutInflater li, ViewGroup vg, Bundle savedInstanceState) {
        View v = li.inflate(R.layout.message_fragment, vg);
        messageList = (ListView) v.findViewById(R.id.messageListView);
        envoyerMessage = (Button) v.findViewById(R.id.buttonEnvoyer);
        editText = (EditText) v.findViewById(R.id.editText);

        envoyerMessage.setOnClickListener(this);

        Intent intent = getActivity().getIntent();
        channelId = intent.getIntExtra(ChannelListActivity.EXTRA_CHANNELID, 0);
        // Toast.makeText(getActivity(), "Channel ID : " + channelId, Toast.LENGTH_SHORT).show();
        SharedPreferences settings = getActivity().getSharedPreferences(LoginActivity.LOGIN_FILE, 0);
        accessToken = settings.getString("accessToken", "None");
        h = new Handler();
        r = new Runnable() {
            @Override
            public void run() {
                // Récupérer message + afficher
                HashMap<String, String> hm = new HashMap<>();
                hm.put("accesstoken", accessToken);
                hm.put("channelid", String.valueOf(channelId));
                hph = new HttpPostHandler();
                hph.addOnDownloadListener(MessageFragment.this);
                hph.execute(new PostRequest("http://www.raphaelbischof.fr/messaging/?function=getmessages", hm));
                if (mustLoop)
                    h.postDelayed(this, 1000);
            }
        };
        if (mustLoop)
            h.post(r);
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mustLoop = false;
    }

    @Override
    public void onClick(View v) {
        if(!editText.getText().toString().equals("")) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("accesstoken", accessToken);
            hm.put("channelid", String.valueOf(channelId));
            hm.put("message", editText.getText().toString());
            SendingHandler sh = new SendingHandler();
            sh.addOnSendingListener(this);
            sh.execute(new PostRequest("http://www.raphaelbischof.fr/messaging/?function=sendmessage", hm));
            editText.setText("");
        }
    }

    @Override
    public void onSendComplete(String downloadedContent) {
        Gson gson = new Gson();
        SendingToken sct = gson.fromJson(downloadedContent, SendingToken.class);
        if(sct.getCode().equals("200"))
            Toast.makeText(getActivity().getApplicationContext(), "Message envoyé !!!", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getActivity().getApplicationContext(), "Erreur d'envoi :(", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSendError(String error) {

    }

    @Override
    public void onDownloadComplete(String downloadedContent) {
        Gson gson = new Gson();
        MessageListToken mlt = gson.fromJson(downloadedContent, MessageListToken.class);
        if(mlt.getMessages() != null && getActivity() != null) {
            if(lesMessages.size() == 0 || !lesMessages.get(0).equals(mlt.getMessages().get(0))) {
                ArrayAdapter<MessageChannelToken> aa = new ArrayAdapter<MessageChannelToken>(getActivity(), R.layout.adapter_list_layout, mlt.getMessages()) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View view = inflater.inflate(R.layout.adapter_list_layout, parent, false);
                        TextView tv1 = (TextView) view.findViewById(R.id.text1);
                        tv1.setText(getItem(position).getUsername() + " : " + getItem(position).getMessage());
                        tv1.setTextColor(Color.BLACK);
                        TextView tv2 = (TextView) view.findViewById(R.id.text2);
                        tv2.setText(getItem(position).getDate());
                        tv2.setTextColor(Color.GRAY);
                    /*
                    ImageView iconView = (ImageView) view.findViewById(R.id.imageIcon);
                    if(iconView.getDrawingCache() == null) {
                        iph = new ImagePostHandler();
                        iph.addOnImageDownloadListener(MessageFragment.this);
                        iph.execute(getItem(position).getImageUrl());
                    }
                    iconView.setImageBitmap(bitmapImage);
                    */
                        return view;
                    }
                };
                lesMessages = mlt.getMessages();
                messageList.setAdapter(aa);
            }
        }
    }

    @Override
    public void onDownloadError(String error) {

    }

    @Override
    public void onImageDownloadComplete(Bitmap downloadedContent) {
        bitmapImage = downloadedContent;
    }

    @Override
    public void onImageDownloadError(String error) {

    }
}
