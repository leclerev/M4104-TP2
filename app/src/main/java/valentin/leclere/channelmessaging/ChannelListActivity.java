package valentin.leclere.channelmessaging;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import valentin.leclere.channelmessaging.Channel.ChannelListToken;
import valentin.leclere.channelmessaging.Fragments.ChannelListFragment;
import valentin.leclere.channelmessaging.Fragments.MessageFragment;

public class ChannelListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final String EXTRA_CHANNELID  = "channel_id";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ChannelListFragment channelFragment = (ChannelListFragment) getSupportFragmentManager().findFragmentById(R.id.channel_fragment_ID);
        MessageFragment messageFragment = (MessageFragment) getSupportFragmentManager().findFragmentById(R.id.message_fragment_ID);
        ChannelListToken channelListToken = channelFragment.getChannelListToken();

        if(messageFragment == null || !messageFragment.isInLayout()){
            Intent i = new Intent(getApplicationContext(), MessageActivity.class);
            i.putExtra(EXTRA_CHANNELID, channelListToken.getChannels().get(position).getChannelID());
            startActivity(i);
        } else {
            messageFragment.setChannelId(channelListToken.getChannels().get(position).getChannelID());
        }
    }
}
