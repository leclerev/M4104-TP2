package valentin.leclere.channelmessaging;

import android.animation.Animator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.androidanimations.library.attention.TadaAnimator;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Random;

import valentin.leclere.channelmessaging.ConnectionPost.ConnectionToken;
import valentin.leclere.channelmessaging.ConnectionPost.HttpPostHandler;
import valentin.leclere.channelmessaging.ConnectionPost.OnDownloadListener;
import valentin.leclere.channelmessaging.ConnectionPost.PostRequest;

public class LoginActivity extends Activity implements View.OnClickListener, OnDownloadListener {

    public static final String LOGIN_FILE = "ConnectionFile";
    private EditText identifiant;
    private EditText mdp;
    private Button valider;
    private ImageView myLogo;
    private TextView text;

    private static final String[] textStringArray = {
            "Baby Driver",
            "Shaun of the Dead",
            "Hot Fuzz",
            "The World's End"
    };

    // SITE A VISITER : http://damien.dabernat.fr/android/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        identifiant = (EditText) findViewById(R.id.idText);
        mdp = (EditText) findViewById(R.id.mdpText);
        valider = (Button) findViewById(R.id.boutonValider);
        myLogo = (ImageView) findViewById(R.id.imageView);
        text = (TextView) findViewById(R.id.textView);

        text.setText(textStringArray[new Random().nextInt(textStringArray.length)]);

        final Handler mHandlerTada = new Handler();
        final int mShortDelay = 4000; //milliseconds

        mHandlerTada.postDelayed(new Runnable(){
            public void run(){
                YoYo.with(Techniques.Tada).playOn(myLogo);
                YoYo.with(Techniques.SlideOutRight).duration(750).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        int newInt;
                        do {
                            newInt = new Random().nextInt(textStringArray.length);
                        }while (textStringArray.length > 1 && textStringArray[newInt].equals(text.getText()));

                        text.setText(textStringArray[newInt]);
                        YoYo.with(Techniques.SlideInLeft).duration(750).playOn(text);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                }).playOn(text);
                mHandlerTada.postDelayed(this, mShortDelay);
            }
        }, mShortDelay);
        
        valider.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Animation animSlideLeft = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.fade_out);
        v.startAnimation(animSlideLeft);

        HttpPostHandler hph = new HttpPostHandler();
        hph.addOnDownloadListener(this);
        HashMap<String, String> hm = new HashMap<>();
        hm.put("username", identifiant.getText().toString());
        hm.put("password", mdp.getText().toString());
        hph.execute(new PostRequest("http://www.raphaelbischof.fr/messaging/?function=connect", hm));
    }

    @Override
    public void onDownloadComplete(String downloadedContent) {
        Gson gson = new Gson();
        ConnectionToken resultat = gson.fromJson(downloadedContent, ConnectionToken.class);
        if((resultat.getResponse().toLowerCase()).equals(("OK").toLowerCase())) {
            SharedPreferences settings = getSharedPreferences(LOGIN_FILE, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("accessToken", resultat.getAccesstoken());
            editor.commit();
            Intent intent = new Intent(LoginActivity.this, ChannelListActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(LoginActivity.this, myLogo, "logo").toBundle());
        }
        else {
            //Toast.makeText(getApplicationContext(), "Erreur de connection :(", Toast.LENGTH_SHORT).show();
            Snackbar mySnackbar = Snackbar.make(findViewById(R.id.llBackground),
                    "Erreur de connection :(", Snackbar.LENGTH_SHORT);
            mySnackbar.setAction("Recomencer ?", this);
            mySnackbar.show();
        }
    }

    @Override
    public void onDownloadError(String error) {

    }
}
