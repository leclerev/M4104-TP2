package valentin.leclere.channelmessaging.ImagePost;

import android.graphics.Bitmap;

public interface OnImageDownloadListener {
    void onImageDownloadComplete(Bitmap downloadedContent);
    void onImageDownloadError(String error);
}
