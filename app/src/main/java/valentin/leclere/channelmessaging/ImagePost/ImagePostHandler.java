package valentin.leclere.channelmessaging.ImagePost;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ImagePostHandler extends AsyncTask<String, Void, Bitmap> {
    private ArrayList<OnImageDownloadListener> listeners = new ArrayList<>();

    public void addOnImageDownloadListener(OnImageDownloadListener listener) { this.listeners.add(listener);}

    @Override
    protected Bitmap doInBackground(String... params) {
        return getImage(params[0]);
    }

    @Override
    protected void onPostExecute(Bitmap param) {
        super.onPostExecute(param);
        for(OnImageDownloadListener oidl : listeners)
        {
            oidl.onImageDownloadComplete(param);
        }
    }

    public Bitmap getImage(String url){
        Bitmap icon = Bitmap.createBitmap(5, 5, Bitmap.Config.ARGB_8888);
        try {
            icon = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e){
            return icon;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return icon;
    }
}
